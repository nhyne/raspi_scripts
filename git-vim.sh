cd
rm -f .gitconfig
if test -d 6a489f75326a18cbd70f
then
    rm -rf "6a489f75326a18cbd70f"
fi
git clone https://gist.github.com/6a489f75326a18cbd70f.git
cd 6a489f75326a18cbd70f
mv .gitconfig ~/.gitconfig
rm -rf "6a489f75326a18cbd70f"

if ! (test -d ~/.vim/bundle/Vundle.vim) 
then 
    git clone https://github.com/gmarik/Vundle.vim ~/.vim/bundle/Vundle.vim
fi

case "$OSTYPE" in
    darwin*)    echo "OSX"
    
    ;;
    linux*)
    sudo apt-get install vim
    sudo apt-get install vim-addon-manager
    sudo apt-get install vim-runtime
    sudo apt-get install vim-syntax-go
    ;;
esac

cd
rm -f .vimrc
if test -d a8f2f7d13dc59b158837
then
    rm -rf "a8f2f7d13dc59b158837"
fi
git clone https://gist.github.com/a8f2f7d13dc59b158837.git
cd a8f2f7d13dc59b158837
mv .vimrc ~/.vimrc
rm -rf "a8f2f7d13dc59b158837"